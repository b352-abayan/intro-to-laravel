<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// access to the authenticated user via the Auth Facades
use Illuminate\Support\Facades\Auth;

// Implement the database manipulation
use App\Models\Post;
use App\Models\PostComment;
use App\Models\PostLike;

class PostController extends Controller
{
    // action ti return a view containing a form for a blog post creation
    public function create()
    {
        return view('posts.create');
    }


    // action to receive form data and subsequently store said data in the posts table.
    // Laravel's "Request" class contains the data from the form submission
    public function store(Request $request)
    {
        // if there is an authenticated user.
        if (Auth::user()) {
            // Instantiate a new Post object from the Post model class.
            $post = new Post;

            // define the properties of the $post object using the received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the foreign key value of thr user_id of the new post.
            $post->user_id = (Auth::user()->id);
            // save this post object in the database
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    // action that will return a view showing all blog posts
    public function index()
    {
        // Fetches all records from the table without specific condition
        // $posts = Post::all();

        // Fetches all records from the table with specific conditions (if need to build more complex queries).
        // $posts = Post::where('isActive', true)->get();
        // Retrive all active posts using scope 
        $posts = Post::active()->get();
        // with() methos is used to pass data to views file.
        // this assigns a variable name ('posts') and it corresponding data ($posts collection) to views.
        return view('posts.index')->with('posts', $posts);
    }

    // s02 activity
    // action that will return a view showing 3 random blog post
    public function welcome()
    {
        // Get all posts in random order
        $posts = Post::active()->inRandomOrder()->limit(3)->get();
        return view('welcome')->with('posts', $posts);
    }

    // action  for showing only the posts that is authored by the authenticated user.
    public function myPosts()
    {
        if (Auth::user()) {
            // retrieve the posts authored by the current logged in user
            // If you have successfully established the model relationship, you can access related data.
            $posts = Auth::user()->posts;
            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    // action for showing a view of a specific post using the URL parameter 'id' to query for the database entry to be shown.
    // Laravel automatically captures the value from the URL parameter and passes it as an argument to the corresponding controller method.
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    // action for showing form for editing the specific post
    public function edit($id)
    {
        // Condition wherein only authenticated user is allowed to show edit form
        if (Auth::user()) {
            // Condition wherein only owned post can be edited by authenticated user. else, editing post that doesn't owned by authenticated user will automatically redirected to /posts endpoint.
            // FindOrFail(id) takes an id and returns a single model. If no matching model exist, it throws an error 404.
            $post = Post::findOrFail($id);
            if (Auth::user()->id !== $post->user_id) {
                return redirect('/posts');
            } else {
                return view('posts.edit')->with('post', $post);
            }
        } else {
            return redirect('/login');
        }
    }

    // action that will overwrite an existing post with matching id.
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        // if authenticated user's ID is the same as the post's user_id
        if (Auth::user()->id == $post->user_id) {
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }
        return redirect('/posts');
    }

    // action that will delete a specific post based on the given id.
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        if (Auth::user()->id == $post->user_id) {
            $post->delete();
        }
        return redirect('/posts');
    }

    // Activity s04
    // action that will temporary delete a specific post based on the given id.
    public function archive($id)
    {
        $post = Post::findOrFail($id);
        if (Auth::user()->id == $post->user_id) {
            $post->isActive = false;
            $post->save();
        }
        return redirect('/posts');
    }

    // action that will allow an authenticated user who is not the post author to toggle a like on the post being viewed.
    public function like($id)
    {
        $post = Post::findOrFail($id);
        $user_id = Auth::user()->id;
        // if authenticated user is not a  post author
        if ($post->user_id != $user_id) {
            // check if a post like has been made by this user before
            if ($post->likes->contains('user_id', $user_id)) {
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            } else {
                // like
                $postLike = new PostLike();
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;
                $postLike->save();
            }
            return redirect("/posts/{$id}");
        }
    }

    // action that will allow an authenticated user to comment any post.
    public function comment(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $user_id = Auth::user()->id;
        // Check if the user is authenticated
        if (Auth::user()) {
            // Insert comment to database
            $comment = new PostComment;
            $comment->content = $request->content;
            $comment->post_id = $post->id;
            $comment->user_id = $user_id;
            $comment->save();
            return redirect("/posts/{$id}");
        } else {
            return redirect('/login');
        }
    }
}

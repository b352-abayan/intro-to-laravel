@extends('layouts.app')

@section('content')
<form action="/posts/{{$post->id}}" method="POST" class="px-5">
    @method('PUT')
    @csrf
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" name="title" id="title" class="form-control" value="{{$post->title}}">
    </div>
    <div class="form-group">
        <label for="content">Content</label>
        <textarea name="content" id="content" cols="3" rows="10" class="form-control">{{$post->content}}</textarea>
    </div>
    <div class="mt-2">
        <button type="submit" class="btn btn-primary">Update Post</button>
    </div>
</form>
@endsection
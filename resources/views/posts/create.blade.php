@extends('layouts.app')

@section('content')
<form action="/posts" method="POST">
    @csrf
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" name="title" id="title" class="form-control">
    </div>
    <div class="form-group">
        <label for="content">Content</label>
        <textarea name="content" id="content" cols="3" rows="10" class="form-control"></textarea>
    </div>
    <div class="mt-2">
        <button type="submit" class="btn btn-primary">Create Post</button>
    </div>
</form>
@endsection
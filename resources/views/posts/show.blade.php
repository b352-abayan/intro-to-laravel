@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <h2 class="card-title">{{$post->title}}</h2>
        <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
        <p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
        <p class="card-text">{{$post->content}}</p>
        <p>Likes: {{$post->likes->count()}} | Comments: {{$post->comments->count()}}</p>
        @if(Auth::user())
        @if(Auth::id() != $post->user_id)

        <form action="/posts/{{$post->id}}/like" class="d-inline" method="POST">
            @csrf
            @method('PUT')
            @if($post->likes->contains("user_id", Auth::id()))
            <button type="submit" class="btn btn-danger">Unlike</button>
            @else
            <button type="submit" class="btn btn-success">Like</button>
            @endif
        </form>
        @endif
        {{-- Comment button Here--}}
        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
            data-bs-target="#exampleModal">Comment</button>
        @endif
        <div class="mt-3">
            <a href="/posts" class="card-link">View all posts</a>
        </div>
    </div>
</div>
@foreach ($post->comments as $comment)
<div class="card mb-3">
    <div class="card-body">
        <p class="card-text text-center">{{$comment->content}}</p>
        <p class="card-subtitle text-muted text-end">Author: {{$comment->user->name}}</p>
        <p class="card-subtitle text-muted mb-3 text-end">Created at: {{$comment->created_at}}</p>
    </div>
</div>
@endforeach




<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Leave a comment</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="/posts/{{$post->id}}/comment" method="POST">
                @csrf
                <div class="modal-body">
                    <textarea name="content" id="" cols="30" rows="6" class="form-control"
                        placeholder="Type here..."></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
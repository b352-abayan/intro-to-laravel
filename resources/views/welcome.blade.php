{{-- s02 Activity solutions --}}
@extends('layouts.app')

@section('content')
<img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" alt="" width="250px"
    class="mx-auto d-block mb-3">
<h1 class="text-center">Featured Posts:</h1>
{{-- Check if there are any posts to display --}}
@if(count($posts) > 0)
@foreach($posts as $post)
<div class="card text-center mb-3">
    <div class="card-body">
        <h4 class="card-title mb-3">
            <a href="/posts/{{$post->id}}">{{$post->title}}</a>
        </h4>
        <h6 class="card-text mb-3">
            Author: {{$post->user->name}}
        </h6>
        <p class="card-subtitle mb-3 text-muted">
            Create at: {{$post->created_at}}
        </p>
    </div>
</div>
@endforeach
@else
<div>
    <h2>There are no posts to show.</h2>
</div>
@endif
@endsection